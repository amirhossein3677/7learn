<?php
function get_post($return_assoc = 0)
{
    $posts = json_decode(file_get_contents(POST_DB), $return_assoc);
    return (array) $posts;
}

function get_users($return_assoc = 0)
{
    $users = json_decode(file_get_contents(USERS_DB), $return_assoc);
    return $users;
}

function get_cat_post($cat_slug)
{
    $posts = get_post();
    $cat_posts = array();

    foreach ($posts as $post) {
        if ($post->cat != $cat_slug) {
            continue;
        }
        $cat_posts[] = $post;
    }
    return $cat_posts;
}


function get_cats($return_assoc = 0)
{
    $cats = json_decode(file_get_contents(CATEGORIES_DB), $return_assoc);
    return $cats;
}

function get_cat($cat)
{
    $cats = get_cats();
    return $cats->$cat;
}

function is_search()
{
    return isset($_GET['s']) && !empty($_GET['s']);
}

function is_cat()
{
    return isset($_GET['cat']) && !empty($_GET['cat']);
}

function search_posts($keyword)
{
    $posts = get_post();
    $finded_posts = array();

    foreach ($posts as $post) {
        if ((strpos($post->content, $keyword) !== false) || (strpos($post->title, $keyword) !== false)) {
            $post->content = str_replace($keyword, '<span style="color:red;">' . $keyword . '</span>', $post->content);
            $post->title = str_replace($keyword, '<span style="color:red;">' . $keyword . '</span>', $post->title);
            $finded_posts[] = $post;
        }
    }
    return $finded_posts;
}

function save_post(object $post): bool
{
    $posts = get_post(1);
    $posts[] = (array) $post;
    $posts_json = json_encode($posts);
    file_put_contents(POST_DB, $posts_json);
    return true;
}

function is_admin($user_name = 0)
{
    if(!is_logged_in()){
        return false;
    }
    $users = get_users();
    foreach ($users as $user) {
        if ($user->email == $_SESSION['login']) {
            if ($user->role == 'admin') {
                return true;
            }
        }
    }

    return false;
}

function is_logged_in()
{
    return isset($_SESSION['login']);
}

function login($email, $password)
{
    $users = get_users();

    foreach ($users as $user) {
        if ($user->email == $email && $user->password == $password) {
            $_SESSION['login'] = $email;
            return true;
        }
    }
    return false;
}

function site_url($uri = '')
{
    return BASE_URL . $uri;
}

function panel_url($uri = '')
{
    return BASE_URL . 'panel/' . $uri;
}


function get_page_post($posts, $page, &$num_pages)
{
    $num_pages = ceil(sizeof($posts) / POST_PER_PAGE);
    $start = ($page - 1) * POST_PER_PAGE;

    return array_slice($posts, $start, POST_PER_PAGE);
}

function logout()
{
    session_destroy();
    header('location: ' . $_SERVER['HTTP_REFERER']);
}
