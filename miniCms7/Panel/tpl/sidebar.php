<div class="left-side-menu">

<div class="slimscroll-menu">

    <!--- Sidemenu -->
    <div id="sidebar-menu">

        <ul class="metismenu" id="side-menu">

            <li>
                <a href="javascript: void(0);" class="waves-effect">
                    <i class="remixicon-stack-line"></i>
                    <span>داشبورد </span>
                    <span class="menu-arrow"></span>
                </a>
                <ul class="nav-second-level" aria-expanded="false">
                    <li>
                        <a href="<?= panel_url(); ?>">داشبورد</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript: void(0);" class="waves-effect">
                    <i class="remixicon-stack-line"></i>
                    <span>مقالات </span>
                    <span class="menu-arrow"></span>
                </a>
                <ul class="nav-second-level" aria-expanded="false">
                    <li>
                        <a href="<?= panel_url('add-post.php'); ?>">افزودن</a>
                        <a href="#">لیست مقالات</a>
                    </li>
                </ul>
            </li>

        </ul>

    </div>
    <!-- End Sidebar -->

    <div class="clearfix"></div>

</div>
<!-- Sidebar -left -->

</div>