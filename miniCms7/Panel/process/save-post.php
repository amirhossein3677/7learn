<?php
require_once '../../init.php';

if (!is_admin()) {
    header('location: ' . site_url());
}

$redir = site_url('panel/add-post.php');
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if (!empty($_POST) && !empty($_POST['content']) && !empty($_POST['title'])) {

        $post_fields = array(
            'title' => $_POST['title'],
            'cat' => $_POST['cat'],
            'author' => $_POST['author'],
            'content' => $_POST['content'],
        );

        save_post((object) $post_fields);
        header("location:" . $redir . '?result=ok');
    } else {
        header("location:" . $redir . '?result=error');
    }
} else {
    echo 'Invalid request! <br> redirect after 5 seconds...';
    header("refresh: 5; url=" . site_url());
}
