<?php require_once 'tpl/header.php'; 
$cats=get_cats();
?>
<div class="content">

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">افزودن</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="col-md-12">
            <div class="card-box">
                <h4 class="header-title mb-3">Horizontal form</h4>

                <form class="form-horizontal" role="form" action="process/save-post.php" method="POST">
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-3 col-form-label">عنوان پست</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name='title' placeholder="عنوان پست">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="thumb" class="col-sm-3 col-form-label">تصویر بندانگشتی</label>
                        <div class="col-sm-9">
                            <input type="text" id="thumb" class="form-control" style="text-align:left;direction:ltr" name='thumb_url' placeholder="Post Thumbnail">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword3" class="col-sm-3 col-form-label">نویسنده</label>
                        <div class="col-sm-9">
                            <select name="author" class="form-control">
                                <option value="لقمان">لقمان</option>
                                <option value="امیرحسین">امیرحسین</option>
                                <option value="علی">علی</option>
                                <option value="نسرین">نسرین</option>
                                <option value="زهرا">زهرا</option>
                                <option value="تقی">تقی</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="input4" class="col-sm-3 col-form-label">دسته بندی</label>
                        <div class="col-sm-9">
                            <select name="cat" id="input4" class="form-control">
                                <?php foreach ($cats as $slug => $cat) : ?>

                                    <option value="<?= $slug ?>"><?= $cat->title ?></option>
                                <?php endforeach; ?>

                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword5" class="col-sm-3 col-form-label">محتوای پست</label>
                        <div class="col-sm-9">
                            <textarea name="content" class="form-control" cols="30" rows="10"></textarea>

                        </div>
                    </div>
                    <div class="form-group mb-0 justify-content-end row">
                        <div class="col-sm-9">
                            <button type="submit" class="btn btn-info waves-effect waves-light">ذخیره</button>
                        </div>
                    </div>
                </form>
            </div>
        </div> <!-- container -->

    </div> <!-- content -->
    <?php require_once 'tpl/footer.php'; ?>