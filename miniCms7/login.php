<?php
require_once "init.php";

if (isset($_GET['action']) && $_GET['action'] == 'logout') {
    logout();
}

if (is_logged_in()) {
    header('location: ' . site_url());
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">

    <!--favicon icon-->
    <link rel="icon" type="image/png" href="assets/img/favicon.png">

    <title>صفحه ورود</title>

    <!--web fonts-->


    <!--basic styles-->
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/vendor/fontawesome/css/all.min.css" rel="stylesheet">
    <link href="assets/vendor/custom-icon/style.css" rel="stylesheet">
    <link href="assets/vendor/vl-nav/css/core-menu.css" rel="stylesheet">
    <link href="assets/vendor/animate.min.css" rel="stylesheet">
    <link href="assets/vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link href="assets/vendor/owl/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="assets/vendor/owl/assets/owl.theme.default.min.css" rel="stylesheet">
    <link href="assets/vendor/prism/prism.css" rel="stylesheet">

    <!--custom styles-->
    <link href="assets/css/main.css" rel="stylesheet">

    <!--[if (gt IE 9) |!(IE)]><!-->
    <!--<link rel="stylesheet" href="assets/vendor/custom-nav/css/effects/fade-menu.css"/>-->
    <link rel="stylesheet" href="assets/vendor/vl-nav/css/effects/slide-menu.css" />
    <!--<![endif]-->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-136585988-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-136585988-1');
    </script>
</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <!--<div class="branding-wrap">-->
                <!--brand start-->
                <div class="navbar-brand float-left">
                    <a class="" href="<?= site_url() ?>">
                        <img class="logo-light" src="assets/img/logo.png" srcset="assets/img/logo@2x.png 2x" alt="CLab">
                        <img class="logo-dark" src="assets/img/logo-dark.png" srcset="assets/img/logo-dark@2x.png 2x" alt="CLab">
                    </a>
                </div>
                <!--brand end-->
                <!--start responsive nav toggle button-->
                <div class="nav-btn hamburger hamburger--slider js-hamburger ">
                    <div class="hamburger-box">
                        <div class="hamburger-inner"></div>
                    </div>
                </div>
                <!--end responsive nav toggle button-->
                <!--</div>-->


                <!--title-->
                <div class="text-center py-md-5 py-3">
                    <h6 class="mb-0">فرم ورود به سایت <span class="badge badge-pill badge-dark">02</span></h6>
                </div>

                <div class="component-section bg-gray">

                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-md-6">
                                <form method="POST" action="<?= site_url('process/do-login.php') ?>">
                                    <div class="form-group">
                                        <div class="icon-field">
                                            <i class="fa fa-envelope-open-text"></i>
                                            <input type="email" name="email" class="form-control" placeholder="آدرس ایمیل">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="icon-field-right">
                                            <i class="fa fa-eye"></i>
                                            <input type="password" name="password" class="form-control" placeholder="وارد کردن رمز عبور">
                                        </div>
                                    </div>
                                    <input type="hidden" name="referer" value="<?= $_SERVER['HTTP_REFERER'] ?? site_url() ?>">
                                    <button type="submit" class="btn btn-pill btn-primary">ورود</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>


                <!--basic scripts-->
                <script src="assets/vendor/jquery/jquery.min.js"></script>
                <script src="assets/vendor/popper.min.js"></script>
                <script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>
                <script src="assets/vendor/vl-nav/js/vl-menu.js"></script>
                <script src="assets/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
                <script src="assets/vendor/owl/owl.carousel.min.js"></script>
                <script src="assets/vendor/jquery.animateNumber.min.js"></script>
                <script src="assets/vendor/jquery.countdown.min.js"></script>
                <script src="assets/vendor/typist.js"></script>
                <script src="assets/vendor/jquery.isotope.js"></script>
                <script src="assets/vendor/imagesloaded.js"></script>
                <script src="assets/vendor/visible.js"></script>
                <script src="assets/vendor/wow.min.js"></script>
                <script src="assets/vendor/clipboard/clipboard.min.js"></script>
                <script src="assets/vendor/clipboard/clipboard-init.js"></script>
                <script src="assets/vendor/prism/prism.js"></script>

                <!--basic scripts initialization-->
                <script src="assets/js/scripts.js"></script>
            </div>
        </div>
    </div>

</body>

</html>