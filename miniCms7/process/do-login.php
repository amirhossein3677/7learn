<?php
require_once "../init.php";
if ($_SERVER['HTTP_REFERER'] != site_url('login.php') || $_SERVER['REQUEST_METHOD'] != 'POST' || empty($_POST['password']) || empty($_POST['email'])) {
    header('location: ' . site_url('login.php'));
}

[$email, $password, $referer] = [$_POST['email'], $_POST['password'], $_POST['referer']];

if (login($email, $password)) {
    header('location: ' . $referer);
 } else {
    echo "Incorrect User or Password";
}
