<?php
function get_post($return_assoc = 0)
{
    return json_decode(file_get_contents(POST_DB), $return_assoc);
}

function save_post(object $post): bool
{
    $posts = get_post(1);
    $posts[] = (array)$post;
    $posts_json = json_encode($posts);
    file_put_contents(POST_DB, $posts_json);
    return true;
}

function is_admin($user_name = 0)
{
    return true;
}

function site_url($uri = '')
{
    return BASE_URL . $uri;
}
