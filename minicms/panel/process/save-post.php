<?php
require_once '../../init.php';
$redir = site_url('panel/add-post.php');
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if (!empty($_POST) && !empty($_POST['content']) && !empty($_POST['title'])) {
        save_post((object)$_POST);
        header("location:" . $redir . '?result=ok');
    } else {
        header("location:" . $redir . '?result=error');
    }
} else {
    echo 'Invalid request! <br> redirect after 5 seconds...';
    header("refresh: 5; url={$redir}");
}
