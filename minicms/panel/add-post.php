<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="assets/css/style.css" rel="stylesheet" type="text/css">
    <title>min CMS panel -> add post</title>
</head>

<body>
    <div class="main">
        <?php if (isset($_GET['result']) && !empty($_GET['result'])) : ?>
            <?php if ($_GET['result'] == 'ok') : ?>
                <div class="result g">پست دخیره شد</div>
            <?php elseif ($_GET['result'] == 'error') : ?>
                <div class="result r">فرم را کامل کنید</div>
            <?php endif; ?>
        <?php endif; ?>
        <div class="add-post">

            <form action="process/save-post.php" method="post">
                <input type="text" name="title" placeholder="عنوان پست"><br>

                <select name="author">
                    <option value="امیرحسین">امیرحسین</option>
                    <option value="نازنین">نازنین</option>
                    <option value="محمد">محمد</option>
                    <option value="لقمان">لقمان</option>
                    <option value="مریم">مریم</option>
                </select>

                <br>
                <textarea name="content" placeholder="متن خود را وارد کنید"></textarea>

                <br>
                <input type="submit" value="انتشار">


            </form>
        </div>
    </div>
</body>

</html>