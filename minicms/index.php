<?php include_once 'init.php';
$posts = get_post();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="<?= site_url('assets/css/styles.css'); ?>" rel="stylesheet" type="text/css">
    <title>min CMS php expert 3</title>
</head>

<body>
    <div class="main">

        <!-- sidebar -->

        <aside class="sidebar">
            <?php if (is_admin()) : ?>
                <section>
                    <a href="<?= site_url('panel'); ?>">پنل مدیریت</a>
                </section>

            <?php endif; ?>
        </aside>

        <!-- content -->

        <div class="main-content">

            <?php foreach ($posts as $pid => $post) : ?>
                <article class="post-box">
                    <span class="author"><?= $post->author; ?></span>
                    <h2><?= $post->title; ?></h2>
                    <div class="post-content"><?= $post->content; ?></div>
                </article>

            <?php endforeach; ?>
        </div>
        <div class="clear-fix"></div>
    </div>
</body>

</html>