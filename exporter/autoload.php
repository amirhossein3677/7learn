<?php
function my_class($class)
{
    $file_path = BASE_PATH . 'lib' . DIRECTORY_SEPARATOR . "$class.php";
    if (file_exists($file_path) && is_readable($file_path)) {
        include_once $file_path;
    } else {
        //Log "$file_path"
    }
}

spl_autoload_register('my_class');
