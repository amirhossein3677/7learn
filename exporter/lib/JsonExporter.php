<?php

class JsonExporter extends Exporter
{
    public static $ext = 'json';
    public function export()
    {   
        $path="upload/{$this->get_file_name()}." . static::$ext;
        file_put_contents($path, json_encode(array($this->get_data()))); 
        echo "<a href='http://sevenl/exporter/$path' download>Download Your ".static::$ext." File</a><br>";
    }
}
