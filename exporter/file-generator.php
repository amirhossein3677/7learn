<?php
include "init.php";

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if (!isset($_POST['data']) or !isset($_POST['format']) or !isset($_POST['filename'])) {
        die('Invalid data!');
    }

    if (empty($_POST['data']) or empty($_POST['filename'])) {
        die('Invalid data!');
    }

    $White_list_ext = array('all', 'txt', 'pdf', 'csv');

    if (!in_array($_POST['format'], $White_list_ext)) {
        die('Invalid data!');
    }

    $ext = $_POST['format'];
    $data = $_POST['data'];
    $file_name = $_POST['filename'];

    if ($ext == 'all') {
        $exporters = array(
            new TxtExporter($filename),
            new CsvExporter($filename),
            new JsonExporter($filename)
        );
        foreach ($exporters as $exporter) {
            $exporter->import($form_data);
            $exporter->export();
        }
    } else {
        $classname = ucfirst($ext) . "Exporter";
        $export = new $classname($filename);
        $export->import($form_data);
        $export->export();
    }
}
