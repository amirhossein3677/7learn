<style>
    * {
        font-family: tahoma;
    }

    .key {
        color: red;
        font-size: 18px;
    }

    .finger-post {
        color: green;
        font-size: 20px;
        font-weight: bold;
    }

    .arg {
        color: #777;
    }

    .array-box {
        margin: 20px 0 0 100px;
    }
</style>
<?php

function myDump(...$arg)
{


    foreach ($arg as $key => $value) {

        switch (true) {
            case is_null($value):
                echo '<span class="arg"><span class="key">' . $key . '</span> is </span>NULL';
                break;

            case is_bool($value):
                if ($value === true) {
                    echo '<span class="arg"><span class="key">' . $key . '</span> is bool </span><span class="finger-post">=></span> True';
                } else {
                    echo '<span class="arg"><span class="key">' . $key . '</span> is bool </span><span class="finger-post">=></span> False';
                }
                break;

            case is_array($value):
                echo '<span class="arg"><span class="key">' . $key . '</span> is array (size : ' . sizeof($value) . ') ' . '</span><span class="finger-post">=></span>';
                scrolling($value);
                break;

            case is_object($value):
                echo '<span class="arg"><span class="key">' . $key . '</span> is object  (' . get_class($value) . ')</span><span class="finger-post">=></span>';
                scrolling($value);
                break;

            default:
            echo  '<span class="key">' . $key . '</span><span class="arg"> (' . gettype($value) . ')';
            echo is_string($value) ? '(length : ' . strlen($value) . ')' : '';
            echo '</span><span class="finger-post"> =></span> ' . $value;
        }
        echo '<br>';
    }
}


//this function is for scrolling in array and object

function scrolling($val)
{
    echo "<div class='array-box'>";

    foreach ($val as $key => $value) {

        switch (true) {

            case is_null($value):
                echo '<span class="arg"><span class="key">' . $key . '</span> is </span>NULL';
                break;

            case is_bool($value):
                if ($value === true) {
                    echo '<span class="arg"><span class="key">' . $key . '</span> is bool </span><span class="finger-post">=></span> True';
                } else {
                    echo '<span class="arg"><span class="key">' . $key . '</span> is bool </span><span class="finger-post">=></span> False';
                }
                break;

            case is_array($value):
                echo '<span class="arg"><span class="key">' . $key . '</span> is array (size : ' . sizeof($value) . ') ' . '</span><span class="finger-post">=></span>';
                scrolling($value);
                break;

            case is_object($value):
                echo '<span class="arg"><span class="key">' . $key . '</span> is object  (' . get_class($value) . ')</span><span class="finger-post">=></span>';
                scrolling($value);
                break;

            default:
                echo  '<span class="key">' . $key . '</span><span class="arg"> (' . gettype($value) . ')';
                echo is_string($value) ? '(length : ' . strlen($value) . ')' : '';
                echo '</span><span class="finger-post"> =></span> ' . $value;
        }
        echo '<br>';
    }
    echo "</div>";
}
//به این علت از دو تابع استفاده شد چون اگر در تابع اول از ساختار بازگشتی استفاده میشد ایندکس ها بهم می خورد