<?php
/*
|--------------------------------------------------------------------------
| my_dump function is like var_dump
|--------------------------------------------------------------------------
|
| This function has some bugs : 
|
|
| 1: Look at the example below
| $handle = new mysqli('HOST', 'USERNAME', 'PASS', 'DBNAME');      #this is a resource variable
|
|
|
| var_dump function prints Similar to the following values on output => var_dump($handle);
|   object(mysqli)#2 (19) { ["affected_rows"]=> int(0) ["client_info"]=> string(79) "mysqlnd 5.0.12-dev - 20150407 - $Id: 38fea24f2847fa7519001be390c98ae0acafe387 $" ...
|   values is right
|
|    
|
| but my_dump function prints on output => my_dump($handle);
|   object(mysqli)#1 (19) { ["affected_rows"]=> NULL ["client_info"]=> NULL ... 
|   all values NULL !!
|
|
|
| 2: my_dump is NOT support integer(2) in object(stdClass)[2]                 //I do not know what is 2?
|
|
|
|
*/

function my_dump(...$args)
{
    echo '<style>address{display:inline-block;}</style>';
    $tab = '   ';
    $repeat_tab = count(debug_backtrace()) - 1;
    if (count(debug_backtrace()) == 1) {
        echo '<pre><small>' . debug_backtrace()[0]['file'] . ':' . debug_backtrace()[0]['line'] . ':</small>';
    }
    foreach ($args as $arg) {
        switch (gettype($arg)) {
            case 'array':
                echo PHP_EOL;
                echo str_repeat($tab, $repeat_tab++) . '<b>array</b> (size=' . count($arg) . ') ' . PHP_EOL;
                foreach ($arg as $key => $value) {
                    echo str_repeat($tab, $repeat_tab) . '\'' . $key . '\' <font color="#888a85">=></font> ';
                    my_dump($value);
                }
                echo PHP_EOL;
                break;

            case 'object':
                echo PHP_EOL;
                echo str_repeat($tab, $repeat_tab) . '<b>object</b>(' . get_class($arg) . ')' . PHP_EOL;
                foreach ($arg as $key => $value) {
                    echo str_repeat($tab, $repeat_tab + 1) . '<i>public</i> \'' . $key . '\' <font color="#888a85">=></font> ';
                    my_dump($value);
                }
                echo PHP_EOL;
                break;

            case 'string':
                echo '<small>string</small> <font color="#cc0000">\'' . $arg . '\'</font> <i>(length=' . strlen($arg) . ')</i>' . PHP_EOL;
                break;

            case 'boolean':
                echo '<small>boolean</small> ';
                echo $arg ? '<font color="#75507b">true</font>' : '<font color="#75507b">false</font>' . PHP_EOL;
                break;

            case 'double':
                echo '<small>float</small> <font color="#f57900">' . $arg . '</font>' . PHP_EOL;
                break;

            case 'integer':
                echo '<small>int</small> <font color="#4e9a06">' . $arg . '</font>' . PHP_EOL;
                break;

            case 'NULL':
                echo '<font color="#3465a4">null</font> ' . PHP_EOL;
                break;

            case 'resource':
                echo 'resource(3) of type (';
                echo get_resource_type($arg) . ')' . PHP_EOL;
                break;

            case 'resource (closed)':
                echo 'resource(3) of type (';
                echo get_resource_type($arg) . ')' . PHP_EOL;
                break;

            default:
                echo 'this variable is not define !';
                break;
        }
    }
    echo (count(debug_backtrace()) == 1) ? '</pre>' : '';
}