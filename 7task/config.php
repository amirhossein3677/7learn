<?php
define('DB_HOST', 'localhost');
define('DB_NAME', '7task');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('BASE_URL', 'http://sevenl/7task/');

const TASK_TABLE ='tasks';


$task_statuses = [1 => 'Draft', 2 => 'Todo', 3 => 'Done', 4 => 'Archive'];

$connection = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);

if ($connection->connect_errno) {
    die('Connnection failed: ' . $connection->connect_error);
}
$connection->set_charset("utf8");


$where = '';

if (isset($_GET['status']) && !empty($_GET['status'])){
    $where = ' WHERE status='.$_GET['status'];
}


$sql = 'SELECT * FROM ' . TASK_TABLE . $where;
$result = $connection->query($sql);
$tasks = $result->fetch_all(MYSQLI_ASSOC);
