<?php
function site_url($url = '')
{
    return BASE_URL . $url;
}

function delete_task(int $task_id)
{
    $sql = "DELETE FROM " . TASK_TABLE . " WHERE id=" . $task_id;
    $result = $GLOBALS['connection']->query($sql);
    return $result;
}

function get_task()
{

    $where = '';

    if (isset($_GET['status']) && !empty($_GET['status'])) {
        $where = ' WHERE status=' . $_GET['status'];
    }


    $sql = 'SELECT * FROM ' . TASK_TABLE . $where;
    $result = $GLOBALS['connection']->query($sql);
    return  $result->fetch_all(MYSQLI_ASSOC);
}

function add_task($title,$owner){

    $sql = "INSERT INTO " . TASK_TABLE . " (title,owner) VALUES ('$title','$owner')";
    $result = $GLOBALS['connection']->query($sql);
    if($GLOBALS['connection']->errno){
        echo $GLOBALS['connection']->error;
    }else{
        echo 'Task added Successfully!';
    }
}
