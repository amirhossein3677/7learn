<?php
require_once 'config.php';
include_once 'function.php';
include_once 'process.php';
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <title>Task manager UI</title>
  <link rel="stylesheet" href="<?= site_url('assets/css/style.css'); ?>" type="text/css">
</head>

<body>
  <!-- partial:index.partial.html -->

  <div class="page">
    <div class="pageHeader">
      <div class="title">Dashboard</div>
      <div class="userPanel"><i class="fa fa-chevron-down"></i><span class="username">John Doe </span><img src="assets/images/73.jpg" width="40" height="40" /></div>
    </div>
    <div class="main">
      <div class="nav">
        <div class="searchbox">
          <div><i class="fa fa-search"></i>
            <input type="search" placeholder="Search" />
          </div>
        </div>
        <div class="menu">
          <div class="title">Navigation</div>
          <ul>
            <li> <i class="fa fa-home"></i>Home</li>
            <li><i class="fa fa-signal"></i>Activity</li>
            <li class="active"> <i class="fa fa-tasks"></i>Manage Tasks</li>
            <li> <i class="fa fa-envelope"></i>Messages</li>
          </ul>
        </div>
      </div>
      <div class="view">
        <div class="viewHeader">
          <div class="title">Manage Tasks</div>
          <div class="functions">
            <div class="button active" id="addTaslBtn">Add New Task</div>
            <div class="button"><a href="<?= site_url(); ?>">All</a></div>
            <div class="button"><a href="<?= site_url('?status=1'); ?>">Draft</a></div>
            <div class="button"><a href="<?= site_url('?status=2'); ?>">Todo</a></div>
            <div class="button"><a href="<?= site_url('?status=3'); ?>">Done</a></div>
            <div class="button"><a href="<?= site_url('?status=4'); ?>">Archive</a></div>
            <div class="button inverz"><i class="fa fa-trash-o"></i></div>
          </div>
        </div>
        <div class="content">
          <div class="list">
            <div class="title">Today</div>
            <ul>
              <?php foreach ($tasks as $task) : $task = (object) $task; ?>
                <li class="checked">
                  <?php if ($task->status == 3) : ?>
                    <i class="fa fa-check-square-o"></i>
                  <?php else : ?>
                    <i class="fa fa-square-o"></i>
                  <?php endif; ?>
                  <span><?= $task->title; ?></span>
                  <div class="info">
                    <div class="button green">
                      <?= $task_statuses[$task->status]; ?>
                    </div>
                    <a href="<?= site_url('?del=' . $task->id); ?>"><i class="fa fa-trash-o"></i></a>
                    <span>Created at <?= $task->created_at; ?></span>
                  </div>
                </li>
              <?php endforeach; ?>
            </ul>
          </div>
          <div class="list">
            <div class="title">Tomorrow</div>
            <ul>
              <li><i class="fa fa-square-o"></i><span>Find front end developer</span>
                <div class="info"></div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- ------ Modal ------  -->
  <div class="modal-wrapper" fadeIn>
    <div class="modal-inner">
    <span class="close">X</span>
      <form action="<?= site_url('ajax-process.php'); ?>" class="form-ajax" method="POST">
        <input type="text" name="title" placeholder="title" id="task-text" required>
        <select name="owner">
          <option value="Ali">Ali</option>
          <option value="Taghi">Taghi</option>
          <option value="Maryam">Maryam</option>
          <option value="Sara">Sara</option>
        </select>
        <input type="submit" value="Add Task"><br><br>
        <div class="result"></div>
      </form>
    </div>
  </div>

  <!-- partial -->
  <script src='assets/js/jquery-2.1.3.min.js'></script>
  <script src="assets/js/script.js"></script>


  <script>
    $('.modal-inner .close').click(function(){
      $('.modal-wrapper').slideUp();
    });

    $('#addTaslBtn').click(function(){
      $('.modal-wrapper').slideDown();
    });

    $('.form-ajax').submit(function(e){
      e.preventDefault();
      var form = $(this);
      var resultBox = form.find('.result');
      resultBox.html('Loading ...');
      $.ajax({
        type:'POST',
        url:form.attr('action'),
        data:form.serialize(),
        timeout: 20000,
        success:function(response){
          resultBox.html(response);
        },
        error:function(){
          resultBox.html('ERROR');
        }
      });



    });
  
  
  </script>

</body>

</html>