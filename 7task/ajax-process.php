<?php
require_once 'config.php';
include_once 'function.php';
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {


    if (strlen($_POST['title']) <= 1) {
        die('Invalid title!');
    } else {
        $_POST['title'] = htmlspecialchars($_POST['title']);
        $owner_whitelist = ['Ali', 'Taghi', 'Sara', 'Maryam'];
        if (in_array($_POST['owner'], $owner_whitelist)) {
            add_task($_POST['title'], $_POST['owner']);
        } else {
            echo 'Error : Invalid Owner!';
        }
    }


} else {
    die('Invalid Request!');
}
