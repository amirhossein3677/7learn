<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="assets/css/style.css" rel="stylesheet">
    <script src="assets/js/jquery.min.js"></script>
    <title>Contact us</title>
</head>

<body>
    <div class="main">

        <div class="title box">
            <h1>تماس با مدیر سایت</h1>
        </div>

        <div class="box">
            <form action="sender.php" method="post">
                <label>نام : </label>
                <input type="text" name="name" class="fields fields-info" required placeholder="Name">
                <br>
                <label>موضوع : </label>
                <input type="text" name="subject" class="fields fields-info" required placeholder="Subject">
                <br>
                <label>ایمیل : </label>
                <input type="email" name="email" class="fields fields-info" required placeholder="E-mail">
                <br>
                <label style="vertical-align: 190px;">متن : </label>
                <textarea name="email_text" class="fields fields-text" required placeholder="Text"></textarea>

                <br><br>
                <label></label>
                <input type="submit" value="ارسال" class="btn btn-green" id='submit'>
                <input type="reset" value="پاک کردن" class="btn btn-red">

            </form>
            <div class="result"></div>
        </div>
    </div>

    <script>
        $("#submit").click(function() {
            $(".result").html('<img src="assets/image/puff-1.svg">');
            $.ajax({
                type: 'post',
                url: "sender.php",
                data: $("form").serialize(),
                success: function(amir) {
                    $(".result").html(amir);
                }
            });
            return false;
        })
    </script>

</body>

</html>