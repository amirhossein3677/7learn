<?php
if ($_SERVER['REQUEST_METHOD'] !== 'POST' or empty($_POST)) {
    header('location:index.php');
}

if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    $to = "amirhosseinhadavand@yahoo.com";
    $name = $_POST['name'];
    $subject = $_POST['subject'];
    $from = $_POST['email'];
    $msg = $name . PHP_EOL . $_POST['email_text'];

    $headers = "From:" . $from . "\r\n";

    echo (mail($to,$subject,$msg,$headers)) ? 'پیام شما ارسال شد' : 'پیام شما ارسال نشد';

}else{
    header('location:index.php');
}
