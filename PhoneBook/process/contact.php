<?php
include_once '../bootstrap/init.php';

/* echo '<pre>';
var_dump($_POST);
echo '</pre>'; */

$action_Whitelist = array('add', 'remove', 'update', 'report');
$category_white_list = array('favorits', 'work', 'family', 'friends', 'hidden');


if (is_post_request() && in_array($_POST['action'], $action_Whitelist)) {

    $action = $_POST['action'];

    switch ($action) {
        case 'add':

            if (!in_array($_POST['category'], $category_white_list)) {
                $_POST['category'] = NULL;
            }

            $new_contact = array(
                'contact_name' => $_POST['contact_name'],
                'contact_number' => $_POST['contact_number'],
                'category' => $_POST['category']
            );
            if ($cdb->add($new_contact)) {
                echo 'Added Successfully :)';
            } else {
                'Added Error :(';
            }
            break;

        case 'remove':
            $cdb->remove(['id[=]' => $_POST['id']]);

            break;



        default:
            echo 'Action not implemented yet :(';
    }
} else {
    die('Invalid Request! :(');
}
