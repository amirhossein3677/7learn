<?php

function asset($asset)
{
    $asset_path = BASE_URL . 'themes/' . ACTIVE_THEME . "/$asset";
    return $asset_path;
}

function site_url($uri = '')
{ 
    return BASE_URL . $uri;
}

function is_post_request()
{
    return ($_SERVER['REQUEST_METHOD'] == 'POST');
}

function get_config($key)
{
    $db_config = array(
        "db" => array(
            'database_type' => 'mysql',
            'database_name' => 'phonebook',
            'server' => 'localhost',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_general_ci',
            'port' => 3306,
            'logging' => true,
        )
    );

    return $db_config[$key];
}
