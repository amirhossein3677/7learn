<?php
include_once  'constants.php';
include_once  BASE_PATH_7L . 'vendor/autoload.php';
include_once  BASE_PATH . 'Utilities/View.php';
include_once  BASE_PATH . 'bootstrap/helpers.php';
include_once  BASE_PATH . 'DB/Base_Model.php';
include_once  BASE_PATH . 'DB/Contact.php';

/* Database Connection */

$cdb = new Contact(get_config('db'));