<html>

<head>
    <link rel="stylesheet" href="<?= asset('style.css') ?>" type="text/css">
    <title>7Contact Manager</title>
</head>

<body>
    <header>
        <h1>Contact Manager</h1>
    </header>
    <main>
        <ul class="contacts">
            <?php foreach ($contacts as $contact) : ?>
                <li class="contact">
                    <span class="delete contact-delete" data-id="<?= $contact['id'] ?>"></span>
                    <h3><?= $contact['contact_name']; ?></h3>
                    <p class="email">email is empty</p>
                    <p class="phone"><?= $contact['contact_number']; ?></p>
                </li>
            <?php endforeach; ?>
        </ul>
    </main>
    <footer>
        <div class="fab addContactBtn"></div>
    </footer>

    <!-- -------- modal ------------ -->

    <div class="modal-wrapper">
        <div class="modal-inner">
            <span class="close">X</span>
            <form action="<?= site_url("process/contact.php") ?>" id="addForm">
                <input type="hidden" name="action" value="add">
                <input type="text" name="contact_name" placeholder="contact name" required>
                <input type="text" name="contact_number" placeholder="contact number" required>
                <select name="category">
                    <option value="">categories</option>
                    <option value="favorite">favorite</option>
                    <option value="work">work</option>
                    <option value="family">family</option>
                    <option value="friends">friends</option>
                    <option value="hidden">hidden</option>
                </select>
                <button type="submit">Add</button>
                <div class="result"></div>
            </form>
        </div>
    </div>

    <script src="<?= asset('assets/jquery.min.js') ?>"></script>
    <script>
        // $("CSS Selector").event(.modal - wrapper);
        $(".modal-wrapper .close").click(function() {
            $(".modal-wrapper").fadeOut();
        });

        $(".addContactBtn").click(function() {
            $(".modal-wrapper").slideDown();
        });

        $(".contact-delete").click(function(e) {
            var btn = $(this);
            $.ajax({
                type: 'POST',
                url: '<?= site_url("process/contact.php") ?>',
                data: {
                    action: 'remove',
                    id: btn.attr('data-id')
                },
                timeout: 20000,
                success: function(response) {
                    // alert(response);
                },
                error: function() {
                    alert("Error");
                }
            });

        });

        $("#addForm").submit(function(e) {
            e.preventDefault();
            var form = $(this);
            var resultBox = form.find('.result');
            resultBox.html("<img src='assets/loading.gif'>");
            $.ajax({
                type: 'POST',
                url: form.attr('action'),
                data: form.serialize(),
                timeout: 20000,
                success: function(response) {
                    resultBox.html(response);
                },
                error: function() {
                    resultBox.html("Error");
                }
            });

        });
    </script>


</body>

</html>