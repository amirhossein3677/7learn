<?php

use Medoo\Medoo;

interface CRUD
{
    public function add($data);
    public function remove($where);
    public function get_rows($columns = '*',$where = '');
   // public function edit($data,$where);
}

class Base_Model extends Medoo implements CRUD
{
    public static $table;
    public static $primary_key = 'id';

    public function add($data)
    {
        $this->insert(static::$table, $data);
        return $this->id();
    }

    public function remove($where)
    {
        $this->delete(static::$table, $where);
    }

    public function get_rows($columns = '*',$where = ''){
        return $this->select(static::$table, $columns, $where);
    }
}
